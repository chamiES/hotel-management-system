/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelmanagementsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Chamidu Chanaka
 */
public class LoginClass {

    Connection conn;

    LoginClass() {
        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");
            System.out.println("Connection passed");

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public boolean LoginCheck(String currentUser, String currentPass) {
        boolean check = false;

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT * FROM Login WHERE UserName ='" + currentUser + "' AND Password = '" + currentPass + "' ";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                check = true;
            }

            if (check == false) {
                JOptionPane.showMessageDialog(null, "Didn't match username and password");

            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return check;
    }

    public void Register(String SSN, String FirstName, String LastName, String Email, String Country, String Gd) {
        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");
            // System.out.println("Connection passed");

            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO Customer VALUES('" + SSN + "','" + FirstName + "','" + LastName + "','" + Email + "','" + Country + "','" + Gd + "')");
            JOptionPane.showMessageDialog(null, "REGISTRATION SUCCESSFUL");

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void Reservation(String SSN, String HotelId, String ReservationId, String RoomNO, String NoDates, String StartDate) {
        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");
            // System.out.println("Connection passed");

            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO Reservation VALUES('" + SSN + "','" + HotelId + "','" + ReservationId + "','" + RoomNO + "','" + NoDates + "','" + StartDate + "')");
            JOptionPane.showMessageDialog(null, "<< DONE..! >>");

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public boolean SSNCheck(String SSN) {
        boolean check = false;

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT * FROM Customer WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                check = true;
            }

            if (check == false) {
                JOptionPane.showMessageDialog(null, "Wrong SSN....!");

            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return check;
    }

    public String ShowFname(String SSN) {
        String Fname = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT FirstName FROM Customer WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Fname = rs.getString("FirstName");
                System.out.println(Fname);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return Fname;
    }

    public String ShowLname(String SSN) {
        String Lname = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT LastName FROM Customer WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Lname = rs.getString("LastName");
                System.out.println(Lname);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return Lname;
    }

    public String ShowCountry(String SSN) {
        String country = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT Country FROM Customer WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                country = rs.getString("Country");
                System.out.println(country);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return country;
    }

    public String ShowEmail(String SSN) {
        String Email = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT Email FROM Customer WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Email = rs.getString("Email");
                System.out.println(Email);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return Email;
    }

    public String ShowGd(String SSN) {
        String gd = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT Gd FROM Customer WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                gd = rs.getString("Gd");
                System.out.println(gd);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return gd;
    }

    public String ShowResrvationId(String SSN) {
        String Id = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT ReservationId FROM Reservation WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Id = rs.getString("ReservationId");
                System.out.println(Id);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return Id;
    }

    public String ShowReservationdate(String SSN) {
        String date = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT StartDate FROM Reservation WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                date = rs.getString("StartDate");
                System.out.println(date);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return date;
    }

    public String ShowRoomNo(String SSN) {
        String No = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT RoomNumber FROM Reservation WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                No = rs.getString("RoomNumber");
                System.out.println(No);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return No;
    }

    public String ShowDates(String SSN) {
        String NoDt = "";

        try {
            // Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");

            Statement st = conn.createStatement();
            String sql = "SELECT NumberOfDates FROM Reservation WHERE SSN ='" + SSN + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                NoDt = rs.getString("NumberOfDates");
                System.out.println(NoDt);
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        return NoDt;
    }

    public String Amount(String dates) {
        int date = Integer.parseInt(dates);
        int total = date * 1500;
        String amt = Integer.toString(total);
        return amt;
    }

    public void Payment(String SSN, String FirstName, String LastName, String ReservationId, String ReservationDate, String RoomNumber, String NumberOfDates, String ReservationDescription, String Amount) {
        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:HMS");
            // System.out.println("Connection passed");

            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO Payments VALUES('" + SSN + "','" + FirstName + "','" + LastName + "','" + ReservationId + "','" + ReservationDate + "','" + RoomNumber + "','" + NumberOfDates + "','" + ReservationDescription + "','" + Amount + "')");
            JOptionPane.showMessageDialog(null, "<< DONE..! >>");

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }

}
